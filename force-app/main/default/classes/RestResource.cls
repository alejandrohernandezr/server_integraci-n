/**
 * @description       : 
 * @last modified on  : 10-29-2021
 * @last modified by  : Alejandro Hernández
**/

//  token: d4s0s6iW1z2BJKKbUuCWvh7s

// token cliente 00D7Q000000KOoF!ARoAQLGTkv_Rh42N0.lie8naKvhh3ygHitRwOM6d0uVtPOqRPQDHm8TZR07yqKEolrI.oAYBqQn5to0kfg.TLWbS2ZxkULQh
@RestResource(urlMapping='/v1/AccountInfo/*')
global with sharing class RestResource {
  
    @HttpGet
    global static Account doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String accountId = EncodingUtil.urlDecode(req.requestURI.substring(req.requestURI.lastIndexOf('/')+1), 'UTF-8');
        Account result = [SELECT Id, Name, Phone, BillingAddress, Website FROM Account WHERE Phone = :accountId LIMIT 1];
        return result;
    }
  
}